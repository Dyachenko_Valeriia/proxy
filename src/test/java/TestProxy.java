import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TestProxy {
    @Test
    public void testReceivingUser() {
        Proxy proxy = new Secretary(new Director(true, true));
        assertEquals("Директор готов вас принять, можете войти", proxy.receivingUser());
    }

    @Test
    public void testReceivingUser1() {
        Proxy proxy = new Secretary(new Director(true, false));
        assertEquals("Директор сейчас занят, в приёме отказано", proxy.receivingUser());
    }

    @Test
    public void testReceivingUser2() {
        Proxy proxy = new Secretary(new Director(false, true));
        assertEquals("Директора нет на рабочем месте", proxy.receivingUser());
    }
}
