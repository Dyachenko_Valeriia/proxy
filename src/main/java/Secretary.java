public class Secretary implements Proxy {
    private Director director;

    public Secretary(Director director) {
        this.director = director;
    }

    @Override
    public String receivingUser() {
        if (director.isAtWork()) {
            return director.receivingUser();
        } else return "Директора нет на рабочем месте";
    }
}
