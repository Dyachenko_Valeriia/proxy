public class Director implements Proxy {
    private boolean atWork; // на работе или нет
    private boolean free; //свободен или нет

    public Director(boolean atWork, boolean free) {
        this.atWork = atWork;
        this.free = free;
    }

    public boolean isAtWork() {
        return atWork;
    }

    public void setAtWork(boolean atWork) {
        this.atWork = atWork;
    }

    public boolean isFree() {
        return free;
    }

    public void setFree(boolean free) {
        this.free = free;
    }

    @Override
    public String receivingUser() {
        if (isFree()) {
            return "Директор готов вас принять, можете войти";
        } else {
            return "Директор сейчас занят, в приёме отказано";
        }
    }
}
